package ro.tuc.ds2020.controllers;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.services.DoctorService;
import javax.validation.Valid;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctor")
public class DoctorController {

    private final DoctorService doctorService;

    @Autowired
    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping(value = "/testAll")
    public void testAll(){
        System.out.println("testAll - Controller");
        doctorService.testAll();
    }

    @GetMapping(value = "/findAllPatients")
    public ResponseEntity<List<PatientDTO>> findAllPatients(){
        System.out.println("REQ+FIND_ALL_PATENTS");
        List<PatientDTO> patientDTOList = doctorService.findAllPatients();
        System.out.println(patientDTOList.toString());
        return new ResponseEntity<>(patientDTOList, HttpStatus.OK);
    }

    @GetMapping(value = "/findAllCaregivers")
    public ResponseEntity<List<CaregiverDTO>> findAllCaregivers(){
        List<CaregiverDTO> caregiverDTOList = doctorService.findAllCaregivers();
        System.out.println(caregiverDTOList);

        return new ResponseEntity<>(caregiverDTOList, HttpStatus.OK);
    }

    @GetMapping(value = "/findAllMedications")
    public ResponseEntity<List<MedicationDTO>> findAllMedications(){
        List<MedicationDTO> medicationDTOList = doctorService.findAllMedications();

        return new ResponseEntity<>(medicationDTOList, HttpStatus.OK);
    }

    @PostMapping(value = "/createAccount")
    public ResponseEntity<UUID> createAccount(@Valid @RequestBody AccountDTO accountDTO){
        System.out.println("Creating account:" + accountDTO.toString());
        UUID accountId = null;
        if(accountDTO.getType().equals("patient")) {
            accountId = doctorService.createAccount(accountDTO);
        }
        else if(accountDTO.getType().equals("caregiver")){
            accountId = doctorService.createCaregiverAccount(accountDTO);
        }
        return new ResponseEntity<>(accountId, HttpStatus.OK);
    }


    @GetMapping(value = "/findPatientById")
    public ResponseEntity<PatientDTO> findPatient(@Valid @RequestBody UUID patientId){

        PatientDTO patientDTO = doctorService.findPatientById(patientId);

        return new ResponseEntity<>(patientDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/createPatient")
    public ResponseEntity<UUID> createPatient(@Valid @RequestBody PatientDTO patientDTO){
        System.out.println(patientDTO);

        UUID patientID = doctorService.createPatient(patientDTO);
        return new ResponseEntity<>(patientID, HttpStatus.CREATED);
       // return new ResponseEntity<>(new PatientDTO().getId(), HttpStatus.CREATED);
    }

    @PutMapping(value = "/updatePatient")
    public ResponseEntity<UUID> updatePatient(@Valid @RequestBody PatientDTO patientDTO) {
        UUID patientId = doctorService.updatePatient(patientDTO);
        return new ResponseEntity<>(patientId, HttpStatus.OK);
    }

    @DeleteMapping(value = "/deletePatient/{id}")
    public ResponseEntity<UUID> deletePatient(@PathVariable("id") UUID patientId){
        System.out.println(patientId);
        patientId = doctorService.deletePatient(patientId);
        return new ResponseEntity<>(patientId, HttpStatus.OK);
    }

    @PostMapping(value = "/createCaregiver")
    public ResponseEntity<UUID> createCaregiver(@Valid @RequestBody CaregiverDTO caregiverDTO){
        System.out.println(caregiverDTO);

        UUID caregiverId = doctorService.createCaregiver(caregiverDTO);
        return new ResponseEntity<>(caregiverId, HttpStatus.CREATED);
        // return new ResponseEntity<>(new PatientDTO().getId(), HttpStatus.CREATED);
    }

    @PutMapping(value = "/updateCaregiver")
    public ResponseEntity<UUID> updateCaregiver(@Valid @RequestBody CaregiverDTO caregiverDTO) {
        UUID caregiverId = doctorService.updateCaregiver(caregiverDTO);
        return new ResponseEntity<>(caregiverId, HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteCaregiver/{id}")
    public ResponseEntity<UUID> deleteCaregiver(@PathVariable("id") UUID caregiverId){
        System.out.println(caregiverId);
        caregiverId = doctorService.deleteCaregiver(caregiverId);
        return new ResponseEntity<>(caregiverId, HttpStatus.OK);
    }

    @PostMapping(value = "/addMedication")
    public ResponseEntity<UUID> addMedication(@Valid @RequestBody MedicationDTO medicationDTO){
        System.out.println(medicationDTO);

        UUID medicationId = doctorService.createMedication(medicationDTO);
        return new ResponseEntity<>(medicationId, HttpStatus.CREATED);
        // return new ResponseEntity<>(new PatientDTO().getId(), HttpStatus.CREATED);
    }

    @PutMapping(value = "/updateMedication")
    public ResponseEntity<UUID> updateMedication(@Valid @RequestBody MedicationDTO medicationDTO) {
        UUID medicationId = doctorService.updateMedication(medicationDTO);
        return new ResponseEntity<>(medicationId, HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteMedication/{id}")
    public ResponseEntity<UUID> deleteMedication(@PathVariable("id") UUID medicationId){
        System.out.println(medicationId);
        medicationId = doctorService.deleteMedication(medicationId);
        return new ResponseEntity<>(medicationId, HttpStatus.OK);
    }


    @PostMapping(value = "/createMedicationPlan")
    public ResponseEntity<UUID> createMedicationPlan(@Valid @RequestBody MedicationPlanDTO medicationPlanDTO){
        System.out.println("Creating medication plan:" + medicationPlanDTO.toString());
        UUID medicationPlanDTOId = doctorService.createMedicationPlan(medicationPlanDTO);
        return new ResponseEntity<>(medicationPlanDTOId, HttpStatus.OK);
    }


    @GetMapping(value = "/searchPatient/{searchPatient}")
    public Set<PatientDTO> searchPatient(@PathVariable("searchPatient") String searchPatient){

        System.out.println("Searching for a patient");

        return doctorService.searchPatient(searchPatient);
    }
    @GetMapping(value = "/searchCaregiver/{searchCaregiver}")
    public Set<CaregiverDTO> searchCaregiver(@PathVariable("searchCaregiver") String searchCaregiver){

        System.out.println("Searching for a Caregiver");

        return doctorService.searchCaregiver(searchCaregiver);
    }
    @GetMapping(value = "/searchMedication/{searchMedication}")
    public Set<MedicationDTO> searchMedication(@PathVariable("searchMedication") String searchMedication){

        System.out.println("Searching for a Medication");

        return doctorService.searchMedication(searchMedication);
    }
}