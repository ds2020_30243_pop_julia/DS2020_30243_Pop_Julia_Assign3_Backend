package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.services.DoctorService;
import ro.tuc.ds2020.services.RPCServer;
import ro.tuc.ds2020.servicesInterfaces.IRPCServer;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/rpc")
public class RPCController {

    private final IRPCServer rpcServer;

    @Autowired
    public RPCController(RPCServer rpcServer) {
        this.rpcServer = rpcServer;
    }

    @GetMapping(value = "/start")
    public ResponseEntity<Integer> startConsumer () throws Exception {
        System.out.println("Starting RPC");
        rpcServer.start();

        return new ResponseEntity<>(1, HttpStatus.OK);

    }
}
