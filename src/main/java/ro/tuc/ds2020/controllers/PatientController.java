package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.services.PatientService;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping(value = "/getPatient/{username}")
    public ResponseEntity<PatientDTO> getPatient(@PathVariable("username") String username){
        PatientDTO patientDTO = patientService.findPatientByUsername(username);

        return new ResponseEntity<>(patientDTO, HttpStatus.OK);
    }
}
