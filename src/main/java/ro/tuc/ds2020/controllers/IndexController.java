package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.servicesInterfaces.IAccountService;

import javax.validation.Valid;
import java.util.UUID;


@RestController
@CrossOrigin
@RequestMapping(value = "/index")
public class IndexController {


    IAccountService accountService;

    @Autowired
    public IndexController(IAccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping(value = "/login")
    public ResponseEntity<AccountDTO> login(@Valid @RequestBody AccountDTO accountDTO){
        System.out.println("Login get: " + accountDTO.toString() + '\n');
        AccountDTO returnDTO = accountService.login(accountDTO.getUsername(), accountDTO.getPassword());
        if(returnDTO == null) {
            return new ResponseEntity<> (returnDTO, HttpStatus.NOT_FOUND);
        }else{
            System.out.println("Login response: " + returnDTO.toString() + '\n');
            return new ResponseEntity<>(returnDTO, HttpStatus.OK);
        }


    }
    @GetMapping(value = "/login2")
    public ResponseEntity<AccountDTO> testRequest(){
        AccountDTO accountDTO = new AccountDTO("a","b",null,"c");
        System.out.println("Login get: " + accountDTO.toString() + '\n');
        AccountDTO returnDTO = accountService.login(accountDTO.getUsername(), accountDTO.getPassword());
        return new ResponseEntity<>(returnDTO, HttpStatus.OK);
    }
}
