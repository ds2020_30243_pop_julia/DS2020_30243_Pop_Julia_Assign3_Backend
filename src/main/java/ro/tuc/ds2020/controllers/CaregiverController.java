package ro.tuc.ds2020.controllers;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.services.ActivityConsumerService;
import ro.tuc.ds2020.services.CaregiverService;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.annotation.SendToUser;

import java.security.Principal;
import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;
    private final ActivityConsumerService activityConsumer;
    @Autowired
    private SimpMessageSendingOperations messagingTemplate;
    @Autowired
    public CaregiverController(CaregiverService caregiverService, ActivityConsumerService activityConsumer) {
        this.caregiverService = caregiverService;
        this.activityConsumer = activityConsumer;
    }

    @MessageMapping("/message")
    @SendToUser("/queue/reply")
    public String processMessageFromClient(@Payload String message, Principal principal) throws Exception {
        String name = new Gson().fromJson(message, Map.class).get("name").toString();
        System.out.println("received " + message);
        messagingTemplate.convertAndSendToUser(principal.getName(), "/queue/reply", name);
       // messagingTemplate.convertAndSend("/queue/reply", "bLA");
        return name;
    }

    @MessageExceptionHandler
    @SendToUser("/queue/errors")
    public String handleException(Throwable exception) {
        return exception.getMessage();
    }

    @GetMapping(value = "/getCaregiver/{username}")
    public ResponseEntity<CaregiverDTO> getCaregiver(@PathVariable("username") String username) {
        CaregiverDTO caregiverDTO = caregiverService.findCaregiverByUsername(username);

        return new ResponseEntity<>(caregiverDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/startConsumer/{host}/{exchange}")
    public ResponseEntity<CaregiverDTO> startConsumer (@PathVariable("host") String host, @PathVariable("exchange") String exchange) throws Exception {
        System.out.println("Starting consuming host,queue:" + host + "," + exchange);
        activityConsumer.startConsumer(host,exchange);
        CaregiverDTO co = new CaregiverDTO();

        return new ResponseEntity<>(co, HttpStatus.OK);

    }

    @GetMapping(value = "/stopConsumer/{host}/{exchange}")
    public ResponseEntity<CaregiverDTO> stopConsumer(@PathVariable("host") String host, @PathVariable("exchange") String exchange){
        System.out.println("Stopped consuming host,queue:" + host + "," + exchange);
        activityConsumer.stopConsumer(host,exchange);
        return new ResponseEntity<>(new CaregiverDTO(), HttpStatus.OK);

    }
}
