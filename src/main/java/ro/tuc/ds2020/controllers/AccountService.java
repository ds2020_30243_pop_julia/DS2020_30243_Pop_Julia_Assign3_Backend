package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.dtos.builders.AccountBuilder;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.repositories.AccountRepository;
import ro.tuc.ds2020.servicesInterfaces.IAccountService;

@Service
public class AccountService implements IAccountService {

    @Autowired
    AccountRepository accountRepository;
    @Autowired
    AccountBuilder accountBuilder;

    @Override
    public AccountDTO login(String username, String password) {

        AccountDTO accountDTO = null;
        Account account = accountRepository.findByUsername(username);

        if(account != null) {
            if(account.getPassword().contentEquals(password)){
                accountDTO = accountBuilder.toAccountDTO(account);
            }
        }

        return accountDTO;
    }
}
