package ro.tuc.ds2020.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import ro.tuc.ds2020.entities.Doctor;

public interface DoctorRepository extends JpaRepository<Doctor, UUID> {
    Doctor findByName(String name);
}
