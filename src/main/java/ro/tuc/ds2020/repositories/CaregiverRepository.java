package ro.tuc.ds2020.repositories;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import ro.tuc.ds2020.entities.Caregiver;

public interface CaregiverRepository extends JpaRepository<Caregiver, UUID>{
    Optional<Caregiver> findByName(String name);
}
