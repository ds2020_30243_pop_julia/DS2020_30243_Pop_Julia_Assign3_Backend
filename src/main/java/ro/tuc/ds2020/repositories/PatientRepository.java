package ro.tuc.ds2020.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import ro.tuc.ds2020.entities.Patient;

public interface PatientRepository extends JpaRepository<Patient, UUID> {
    Patient findByName(String name);

}
