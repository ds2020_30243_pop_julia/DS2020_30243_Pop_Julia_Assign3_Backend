package ro.tuc.ds2020.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import  ro.tuc.ds2020.entities.Account;


public interface AccountRepository extends JpaRepository<Account, UUID>{
    Account findByUsername(String username);
}
