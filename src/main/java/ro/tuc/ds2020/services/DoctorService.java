package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.dtos.builders.*;
import ro.tuc.ds2020.entities.*;
import ro.tuc.ds2020.repositories.*;
import ro.tuc.ds2020.servicesInterfaces.IDoctorService;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class DoctorService implements IDoctorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DoctorService.class);

    @Autowired
    AccountRepository accountRepository;
    @Autowired
    DoctorRepository doctorRepository;
    @Autowired
    AccountBuilder accountBuilder;
    @Autowired
    PatientBuilder patientBuilder;
    @Autowired
    CaregiverBuilder caregiverBuilder;
    @Autowired
    PatientRepository patientRepository;
    @Autowired
    CaregiverRepository caregiverRepository;

    @Autowired
    MedicationBuilder medicationBuilder;
    @Autowired
    MedicationRepository medicationRepository;

    @Autowired
    MedicationPlanBuilder medicationPlanBuilder;
    @Autowired
    MedicationPlanRepository medicationPlanRepository;

    @Override
    @Transactional
    public void testAll(){
/*
        System.out.println("testAll - Service 1");
        Doctor doctor1 = new Doctor();
        Account doctor1Account = new Account();
        doctor1Account.setCaregiver(null);
        doctor1Account.setPatient(null);
        doctor1Account.setPassword("doctor2");
        doctor1Account.setUsername("doctor2");

        doctor1.setAddress("Adresa Doctor2");
        doctor1.setName("Doctor2");
        doctor1.setMedicationPlan(new HashSet<MedicationPlan>());
        doctor1.setDoctorAccount(doctor1Account);

        doctor1 = doctorRepository.save(doctor1);

        System.out.println("testAll - Service 2");
        doctor1Account.setDoctor(doctor1);
        doctor1Account = accountRepository.save(doctor1Account);
        System.out.println("testAll - Service 3");
        doctor1.setDoctorAccount(doctor1Account);
        doctor1 = doctorRepository.save(doctor1);
        System.out.println("testAll - Service 4");
        doctor1.setDoctorAccount(doctor1Account);
        doctor1Account = accountRepository.save(doctor1Account);
        System.out.println("testAll - Service 5");

        Account caregiver1Account = new Account();
        caregiver1Account.setDoctor(null);
        caregiver1Account.setPatient(null);
        caregiver1Account.setPassword("caregiver1");
        caregiver1Account.setUsername("caregiver1");

        Caregiver caregiver1 = new Caregiver();
        caregiver1.setAddress("Adresa Caregiver1");
        caregiver1.setBirthDate("30-30-3030");
        caregiver1.setGender("Male");
        caregiver1.setName("Caregiver 1");
        caregiver1.setCaregiverAccount(caregiver1Account);
        caregiver1.setPatientsList(new HashSet<Patient>());

        caregiver1 = caregiverRepository.save(caregiver1);
        caregiver1Account.setCaregiver(caregiver1);
        accountRepository.save(caregiver1Account);

        caregiver1.setCaregiverAccount(caregiver1Account);
        caregiver1 = caregiverRepository.save(caregiver1);
        caregiver1Account.setCaregiver(caregiver1);
        accountRepository.save(caregiver1Account);

        Account caregiver2Account = new Account();
        caregiver2Account.setDoctor(null);
        caregiver2Account.setPatient(null);
        caregiver2Account.setPassword("caregiver2");
        caregiver2Account.setUsername("caregiver2");

        Caregiver caregiver2 = new Caregiver();
        caregiver2.setAddress("Adresa Caregiver2");
        caregiver2.setBirthDate("40-40-4040");
        caregiver2.setGender("Male");
        caregiver2.setName("Caregiver 2");
        caregiver2.setCaregiverAccount(caregiver2Account);
        caregiver2.setPatientsList(new HashSet<Patient>());

        caregiver2 = caregiverRepository.save(caregiver2);
        caregiver2Account.setCaregiver(caregiver2);
        accountRepository.save(caregiver2Account);

        caregiver2.setCaregiverAccount(caregiver2Account);
        caregiver2 = caregiverRepository.save(caregiver2);
        caregiver2Account.setCaregiver(caregiver2);
        accountRepository.save(caregiver2Account);
        */


    }
    @Override
    public List<PatientDTO> findAllPatients(){
        List<PatientDTO> patientDTOList = new ArrayList<PatientDTO>();
        for(Patient p : patientRepository.findAll()) {
            patientDTOList.add(patientBuilder.toPatientDTO(p));
        }
        return patientDTOList;
    }

    @Override
    public List<CaregiverDTO> findAllCaregivers(){
        List<CaregiverDTO> caregiverDTOList = new ArrayList<CaregiverDTO>();
        System.out.println("\n\n\n~~~~~~~~~~~~~~~~~~~~~~~~CAREGIVERS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n\n");
        for(Caregiver c : caregiverRepository.findAll()) {
            System.out.println(c);
            caregiverDTOList.add(caregiverBuilder.toCaregiverDTO(c));
        }
        return caregiverDTOList;
    }

    @Override
    public UUID createMedicationPlan(MedicationPlanDTO medicationPlanDTO) {
        MedicationPlan medicationPlan = medicationPlanBuilder.toEntity(medicationPlanDTO);
        medicationPlan.setId(null);
        System.out.println(medicationPlan);
        medicationPlan = medicationPlanRepository.save(medicationPlan);
        System.out.println(medicationPlan);
        LOGGER.debug("medicationPlan with id {} was inserted in db", medicationPlan.getId());
        return medicationPlan.getId();
    }

    @Override
    public PatientDTO findPatientById(UUID id){
        Optional<Patient> patientOptional = patientRepository.findById(id);
        if(!patientOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found!");
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + "with id: " + id);
        }
        return patientBuilder.toPatientDTO(patientOptional.get());
    }

    @Override
    public CaregiverDTO findCaregiverById(UUID id){
        Optional<Caregiver> caregiverOptional = caregiverRepository.findById(id);
        if(!caregiverOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found!");
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + "with id: " + id);
        }
        return caregiverBuilder.toCaregiverDTO(caregiverOptional.get());
    }

    @Override
    public MedicationDTO findMedicationById(UUID id){
        Optional<Medication> medicationOptional = medicationRepository.findById(id);
        if(!medicationOptional.isPresent()) {
            LOGGER.error("Medication with id {} was not found!");
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + "with id: " + id);
        }
        return medicationBuilder.toMedicationDTO(medicationOptional.get());
    }

    @Override
    @Transactional
    public UUID createPatient(PatientDTO patientDTO){
        Patient patient = patientBuilder.toEntity(patientDTO);
        patient.setId(null);
        System.out.println(patient);
        patient = patientRepository.save(patient);
        System.out.println(patient);
        LOGGER.debug("Patient with id {} was inserted in db", patient.getId());
        return patient.getId();
    }

    @Override
    public UUID updatePatient(PatientDTO patientDTO){
        PatientDTO patientDTOAux = findPatientById(patientDTO.getId());
        Patient patient = new Patient();

        if(patientDTOAux.getId() == patientDTO.getId()){
            patient = patientBuilder.toEntity(patientDTO);
            patient = patientRepository.save(patient);

            return patient.getId();
        }
        return null;
    }

    @Override
    public UUID deletePatient(UUID id) {
        Optional<Patient> patientOptional = patientRepository.findById(id);
        if(!patientOptional.isPresent()) {
            LOGGER.error("Patient with id {} is not found!", id);
            throw  new ResourceNotFoundException((Patient.class.getSimpleName() + " with id: " + id));
        }
        System.out.println("Deleting patient with id: " + id);
        patientRepository.deleteById(id);
        return id;
    }

    @Override
    @Transactional
    public UUID createCaregiver(CaregiverDTO caregiverDTO){
        Caregiver caregiver = caregiverBuilder.toEntity(caregiverDTO);
        caregiver.setId(null);
        System.out.println(caregiver);
        caregiver = caregiverRepository.save(caregiver);
        System.out.println(caregiver);
        LOGGER.debug("Caregiver with id {} was inserted in db", caregiver.getId());
        return caregiver.getId();
    }

    @Override
    public UUID updateCaregiver(CaregiverDTO caregiverDTO){
        CaregiverDTO caregiverDTOAux = findCaregiverById(caregiverDTO.getId());
        Caregiver caregiver = new Caregiver();

        if(caregiverDTOAux.getId() == caregiverDTO.getId()){
            caregiver = caregiverBuilder.toEntity(caregiverDTO);
            caregiver = caregiverRepository.save(caregiver);

            return caregiver.getId();
        }
        return null;
    }

    @Override
    public UUID deleteCaregiver(UUID id) {
        Optional<Caregiver> caregiverOptional = caregiverRepository.findById(id);
        if(!caregiverOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} is not found!", id);
            throw  new ResourceNotFoundException((Caregiver.class.getSimpleName() + " with id: " + id));
        }
        System.out.println("Deleting caregiver with id: " + id);
        caregiverRepository.deleteById(id);
        return id;
    }

    @Override
    @Transactional
    public UUID createAccount(AccountDTO accountDTO) {
        Account account = accountBuilder.toEntity(accountDTO);
        account.setId(null);
        account = accountRepository.save(account);
        PatientDTO patientDTO = findPatientById(accountDTO.getOwnerId());
        patientDTO.setAccountId(account.getId());
        updatePatient(patientDTO);


        return account.getId();
    }

    @Override
    @Transactional
    public UUID createCaregiverAccount(AccountDTO accountDTO) {
        Account account = accountBuilder.toEntity(accountDTO);
        account.setId(null);
        account = accountRepository.save(account);
        CaregiverDTO caregiverDTO = findCaregiverById(accountDTO.getOwnerId());
        caregiverDTO.setAccountId(account.getId());
        updateCaregiver(caregiverDTO);


        return account.getId();
    }

    @Override
    @Transactional
    public UUID createMedication(MedicationDTO medicationDTO){
        Medication medication = medicationBuilder.toEntity(medicationDTO);
        medication.setId(null);
        System.out.println(medication);
        medication = medicationRepository.save(medication);
        System.out.println(medication);
        LOGGER.debug("Medication with id {} was inserted in db", medication.getId());
        return medication.getId();
    }

    @Override
    public List<MedicationDTO> findAllMedications(){
        List<MedicationDTO> medicationDTOList = new ArrayList<MedicationDTO>();
        System.out.println("\n\n\n~~~~~~~~~~~~~~~~~~~~~~~~Medications~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n\n");
        for(Medication m : medicationRepository.findAll()) {
            System.out.println(m);
            medicationDTOList.add(medicationBuilder.toMedicationDTO(m));
        }
        return medicationDTOList;
    }

    @Override
    public UUID updateMedication(MedicationDTO medicationDTO){
        MedicationDTO medicationDTOAux = findMedicationById(medicationDTO.getId());
        Medication medication = new Medication();

        if(medicationDTOAux.getId() == medicationDTO.getId()){
            medication = medicationBuilder.toEntity(medicationDTO);
            medication = medicationRepository.save(medication);

            return medication.getId();
        }
        return null;
    }

    @Override
    public UUID deleteMedication(UUID id) {
        Optional<Medication> medicationOptional = medicationRepository.findById(id);
        if(!medicationOptional.isPresent()) {
            LOGGER.error("Medication with id {} is not found!", id);
            throw  new ResourceNotFoundException((Medication.class.getSimpleName() + " with id: " + id));
        }
        System.out.println("Deleting medication with id: " + id);
        medicationRepository.deleteById(id);
        return id;
    }

    @Override
    public Set<PatientDTO> searchPatient(String searchString){

        Set<PatientDTO> returnSet = new HashSet<PatientDTO>();

        for(Patient patient: patientRepository.findAll()) {

            if(patient.getName().contains(searchString)) {

                PatientDTO pDTO = patientBuilder.toPatientDTO(patient);

                returnSet.add(pDTO);
            }
        }

        return returnSet;

    }

    @Override
    public Set<CaregiverDTO> searchCaregiver(String searchString) {

        Set<CaregiverDTO> returnSet = new HashSet<CaregiverDTO>();

        for(Caregiver caregiver: caregiverRepository.findAll()) {

            if(caregiver.getName().contains(searchString)) {

                CaregiverDTO cDTO = caregiverBuilder.toCaregiverDTO(caregiver);

                returnSet.add(cDTO);
            }
        }

        return returnSet;
    }

    @Override
    public Set<MedicationDTO> searchMedication(String searchString) {
        Set<MedicationDTO> returnSet = new HashSet<MedicationDTO>();

        for(Medication medication: medicationRepository.findAll()) {

            if(medication.getName().contains(searchString)) {

                MedicationDTO mDTO = medicationBuilder.toMedicationDTO(medication);

                returnSet.add(mDTO);
            }
        }
        System.out.println(returnSet);
        return returnSet;
    }

}