package ro.tuc.ds2020.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rabbitmq.client.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.PatientRepository;
import ro.tuc.ds2020.servicesInterfaces.IRPCServer;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RPCServer implements IRPCServer {

    private int isRunning = 0;
    private static final String RPC_QUEUE_NAME = "rpc_queue";

    private final DoctorService doctorService;

    @Autowired
    public RPCServer(DoctorService doctorService){
        this.doctorService = doctorService;
    }


    @Override
    public void start()  throws Exception{
        if(isRunning == 0) {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("sparrow.rmq.cloudamqp.com");
            factory.setUsername("boyrjkey");
            factory.setPassword("Qyw89sPcD_hNivow241cOH11AT3-5kSe");
            factory.setVirtualHost("boyrjkey");

            try (Connection connection = factory.newConnection();
                 Channel channel = connection.createChannel()) {
                channel.queueDeclare(RPC_QUEUE_NAME, false, false, false, null);
                channel.queuePurge(RPC_QUEUE_NAME);

                channel.basicQos(1);

                System.out.println(" [x] Awaiting RPC requests");
                isRunning++;

                Object monitor = new Object();
                DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                    AMQP.BasicProperties replyProps = new AMQP.BasicProperties
                            .Builder()
                            .correlationId(delivery.getProperties().getCorrelationId())
                            .build();

                    String response = "";

                    try {
                        String message = new String(delivery.getBody(), "UTF-8");
                        if (message.contentEquals("doPrint"))
                            response = doPrint();
                        else {
                            String[] messageContent = message.split("\\+", 2);
                            //for(String s : messageContent)
                            //System.out.println(s);
                            if (messageContent[0].contentEquals("medicationPlan"))
                                response = getMedicationPlanFor(UUID.fromString(messageContent[1]));
                            else if (messageContent[0].contentEquals("takeMedication")) {
                                System.out.println("Medication " + messageContent[1] + " has been taken");
                            } else if (messageContent[0].contentEquals("missedMedication")) {
                                System.out.println("Medication " + messageContent[1] + " was not taken during the intake interval");
                            } else {
                                System.out.println("Invalid call:" + message);
                                response = "Invalid call";
                            }
                        }

                    } catch (RuntimeException e) {
                        System.out.println(" [.] " + e.toString());
                    } finally {
                        channel.basicPublish("", delivery.getProperties().getReplyTo(), replyProps, response.getBytes("UTF-8"));
                        channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                        // RabbitMq consumer worker thread notifies the RPC server owner thread
                        synchronized (monitor) {
                            monitor.notify();
                        }
                    }
                };

                channel.basicConsume(RPC_QUEUE_NAME, false, deliverCallback, (consumerTag -> {
                }));
                // Wait and be prepared to consume the message from RPC client.
                while (true) {
                    synchronized (monitor) {
                        try {
                            monitor.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
    private static String doPrint(){
        System.out.println("doing a print");
        return "doing a print";
    }

    private String getMedicationPlanFor(UUID patientId) {
        System.out.println("Getting medication");
        List<String> medicationsMocked = new ArrayList<String>();
        medicationsMocked.add("Medication X");
        medicationsMocked.add("Medication Y");
        medicationsMocked.add("Medication Z");

        MedicationPlanDTO medicationPlanDTO = new MedicationPlanDTO(
                "08:00:00-11:00:00",
                "Daily",
                "Doctor2",
                "Mocked",
                medicationsMocked);
        try{

            PatientDTO patientDTO =  doctorService.findPatientById(patientId);

            if(patientDTO != null && (patientDTO.getMedicationPlansDTO() != null || !patientDTO.getMedicationPlansDTO().isEmpty())) {
                medicationPlanDTO = patientDTO.getMedicationPlansDTO().stream().findFirst()
                        .orElse(new MedicationPlanDTO(
                                "08:00:00-11:00:00",
                                "Daily",
                                "Doctor2",
                                patientDTO.getName(),
                                medicationsMocked));
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        Gson gson = new GsonBuilder().create();
        return gson.toJson(medicationPlanDTO,MedicationPlanDTO.class);

    }
}