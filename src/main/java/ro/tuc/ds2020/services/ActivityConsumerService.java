package ro.tuc.ds2020.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rabbitmq.client.*;

import com.rabbitmq.tools.jsonrpc.JsonRpcServer;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.Activity;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Date;
import java.util.concurrent.TimeoutException;

@Service
public class ActivityConsumerService {

    private SimpleMessageListenerContainer container;

    @Autowired
    private SimpMessagingTemplate template;

    @Scheduled(fixedRate = 2000)
    public void fireGreeting() {
        this.template.convertAndSend("/queue/reply", ".");
    }

    public ActivityConsumerService() {
        //    this.startConsumer("localhost", "EXCHANGE_NAME");
    }
    public void stopConsumer(String host, String exchange) {
        if(container != null)
        if(container.isActive() || container.isRunning())
            container.stop();
    }

    public void startConsumer(String host, String exchange2)  throws Exception {

        // set up the connection
        CachingConnectionFactory connectionFactory=new CachingConnectionFactory("sparrow.rmq.cloudamqp.com");
        connectionFactory.setUsername("boyrjkey");
        connectionFactory.setPassword("Qyw89sPcD_hNivow241cOH11AT3-5kSe");
        connectionFactory.setVirtualHost("boyrjkey");

        //Recommended settings
        connectionFactory.setRequestedHeartBeat(30);
        connectionFactory.setConnectionTimeout(30000);

        //Set up queue, exchanges and bindings
        RabbitAdmin admin = new RabbitAdmin(connectionFactory);
        Queue queue = new Queue("sensor_data");
        admin.declareQueue(queue);
        TopicExchange exchange = new TopicExchange("caregiver_topic");
        admin.declareExchange(exchange);
        admin.declareBinding(
                BindingBuilder.bind(queue).to(exchange).with("caregiver_topic.*"));

        //Set up the listener
        container =
                new SimpleMessageListenerContainer(connectionFactory);
        Object listener = new Object() {
            public void handleMessage(String message) {
                Gson gson = new GsonBuilder().create();
                Activity activity = gson.fromJson(message, Activity.class);
                try {
                    checkRules(activity);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        };

        //Send a message
        MessageListenerAdapter adapter = new MessageListenerAdapter(listener);
        container.setMessageListener(adapter);
        container.setQueueNames("sensor_data");
        container.start();
    }

    private void checkRules(Activity activity) throws ParseException {
        Date startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(activity.getStart());
        Date endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(activity.getEnd());
        long durationMinutes = Duration.between(startDate.toInstant(), endDate.toInstant()).toMinutes();

        System.out.println("Took: " + durationMinutes + " to complete, which is");
        if (activity.getActivity().contentEquals("Sleeping")) {
            if (durationMinutes > 7*60) {
                System.out.println("bad. WARNING!!! Sleeping > 7h\n");
                this.template.convertAndSend("/queue/reply", "Sleeping > 7h: " + activity.getStart() + " ->" + activity.getEnd() + "\n");
            }
        } else if (activity.getActivity().contentEquals("Leaving")) {
            if (durationMinutes > 5*60) {
                System.out.print("bad. WARNING!!! Leaving > 5h\n");
                this.template.convertAndSend("/queue/reply", "Leaving > 5h: " + activity.getStart() + " ->" + activity.getEnd() + "\n");
            }
        } else if (activity.getActivity().contentEquals("Toileting") || activity.getActivity().contentEquals("Grooming") || activity.getActivity().contentEquals("Showering") ) {
            if (durationMinutes > 30) {
                System.out.print("bad. WARNING!!! Toileting > 30m\n");
                this.template.convertAndSend("/queue/reply", "Bathroom > 30m: " + activity.getStart() + " ->" + activity.getEnd() + "\n");
            }
        }
        else {
            System.out.print("ok\n");
        }
    }
}
