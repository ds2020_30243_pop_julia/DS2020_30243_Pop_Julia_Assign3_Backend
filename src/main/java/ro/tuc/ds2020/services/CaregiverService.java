package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.repositories.AccountRepository;
import ro.tuc.ds2020.servicesInterfaces.ICaregiverService;

@Service
public class CaregiverService implements ICaregiverService {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    CaregiverBuilder caregiverBuilder;

    @Override
    public CaregiverDTO findCaregiverByUsername(String username){
        Account account = accountRepository.findByUsername(username);
        CaregiverDTO caregiverDTO = caregiverBuilder.toCaregiverDTO(account.getCaregiver());

        return caregiverDTO;
    }

}
