package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.repositories.AccountRepository;
import ro.tuc.ds2020.servicesInterfaces.IPatientService;

@Service
public class PatientService implements IPatientService {

    @Autowired
    AccountRepository accountRepository;
    @Autowired
    PatientBuilder patientBuilder;

    @Override
    public PatientDTO findPatientByUsername(String username){
        Account account = accountRepository.findByUsername(username);
        PatientDTO patientDTO = patientBuilder.toPatientDTO(account.getPatient());

        return patientDTO;
    }

}
