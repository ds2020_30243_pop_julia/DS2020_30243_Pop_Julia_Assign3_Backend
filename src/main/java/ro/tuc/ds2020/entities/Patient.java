package ro.tuc.ds2020.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity
public class Patient implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	  @GeneratedValue(generator = "uuid2")
	  @GenericGenerator(name = "uuid2", strategy = "uuid2")
	  @Type(type = "uuid-binary")
	  private UUID id;
	  
	  @Column(name = "name", nullable = false)
	  private String name;
	  
	  @Column(name = "birthDate", nullable = false)
	  private String birthDate;
	  
	  @Column(name = "gender", nullable = false)
	  private String gender;
	  
	  @Column(name = "address", nullable = false)
	  private String address;
	  
	  @Column(name = "medicalRecord", nullable = false)
	  private String medicalRecord;
	  
	  @OneToMany(fetch = FetchType.EAGER ,mappedBy="patient")
	  private Set<MedicationPlan> patientMedicationPlans;
	  
	  @OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	  @JoinColumn(name="account_id")
	  private Account patientAccount;

	  @ManyToOne(fetch = FetchType.EAGER)
	  @JoinColumn(name="caregiver_id", nullable = false)
	  private Caregiver caregiver;

	public Patient(UUID id, String name, String birthDate, String gender, String address, String medicalRecord, Set<MedicationPlan> patientMedicationPlans, Account patientAccount, Caregiver caregiver) {
		this.id = id;
		this.name = name;
		this.birthDate = birthDate;
		this.gender = gender;
		this.address = address;
		this.medicalRecord = medicalRecord;
		this.patientMedicationPlans = patientMedicationPlans;
		this.patientAccount = patientAccount;
		this.caregiver = caregiver;
	}

	public Patient(String name, String birthDate, String gender, String address, String medicalRecord, Set<MedicationPlan> patientMedicationPlans, Account patientAccount, Caregiver caregiver) {
		this.name = name;
		this.birthDate = birthDate;
		this.gender = gender;
		this.address = address;
		this.medicalRecord = medicalRecord;
		this.patientMedicationPlans = patientMedicationPlans;
		this.patientAccount = patientAccount;
		this.caregiver = caregiver;
	}

	public Patient(){

	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMedicalRecord() {
		return medicalRecord;
	}

	public void setMedicalRecord(String medicalRecord) {
		this.medicalRecord = medicalRecord;
	}

	public Set<MedicationPlan> getPatientMedicationPlans() {
		return patientMedicationPlans;
	}

	public void setPatientMedicationPlans(Set<MedicationPlan> patientMedicationPlans) {
		this.patientMedicationPlans = patientMedicationPlans;
	}

	public Account getPatientAccount() {
		return patientAccount;
	}

	public void setPatientAccount(Account patientAccount) {
		this.patientAccount = patientAccount;
	}

	public Caregiver getCaregiver() {
		return caregiver;
	}

	public void setCaregiver(Caregiver caregiver) {
		this.caregiver = caregiver;
	}

	@Override
	public String toString() {
		String returnPatient = "Patient{" +
				"id=" + id +
				", name='" + name + '\'' +
				", birthDate='" + birthDate + '\'' +
				", gender='" + gender + '\'' +
				", address='" + address + '\'' +
				", medicalRecord='" + medicalRecord + '\'' +
				", patientMedicationPlans=" + patientMedicationPlans +
				", patientAccount=" + patientAccount;
		if(this.caregiver!=null)
			returnPatient += ", caregiver=" + caregiver.getName() +
					'}';
		return returnPatient;
	}
}
