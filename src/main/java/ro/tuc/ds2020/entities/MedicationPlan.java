package ro.tuc.ds2020.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity
public class MedicationPlan implements Serializable{
	
	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Type(type = "uuid-binary")
	private UUID id;

	@Column(name = "intakeIntervals", nullable = false)
	private String intakeIntervals;
	
	@Column(name = "treatmentPeriod", nullable = false)
	private String treatmentPeriod;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="doctor_id", nullable=false)
	private Doctor doctor;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="patient_id", nullable=false)
	private Patient patient;
	
	@ManyToMany(fetch = FetchType.EAGER)
	private Set<Medication> medicationList;

	public MedicationPlan(String intakeIntervals, String treatmentPeriod, Doctor doctor, Patient patient, Set<Medication> medicationList) {
		this.intakeIntervals = intakeIntervals;
		this.treatmentPeriod = treatmentPeriod;
		this.doctor = doctor;
		this.patient = patient;
		this.medicationList = medicationList;
	}

	public MedicationPlan(){

	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getIntakeIntervals() {
		return intakeIntervals;
	}

	public void setIntakeIntervals(String intakeIntervals) {
		this.intakeIntervals = intakeIntervals;
	}

	public String getTreatmentPeriod() {
		return treatmentPeriod;
	}

	public void setTreatmentPeriod(String treatmentPeriod) {
		this.treatmentPeriod = treatmentPeriod;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Set<Medication> getMedicationList() {
		return medicationList;
	}

	public void setMedicationList(Set<Medication> medicationList) {
		this.medicationList = medicationList;
	}

	@Override
	public String toString() {
		return "MedicationPlan{" +
				"id=" + id +
				", intakeIntervals='" + intakeIntervals + '\'' +
				", treatmentPeriod='" + treatmentPeriod + '\'' +
				'}';
	}
}
