package ro.tuc.ds2020.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity
public class Doctor implements Serializable{

	  @Id
	  @GeneratedValue(generator = "uuid2")
	  @GenericGenerator(name = "uuid2", strategy = "uuid2")
	  @Type(type = "uuid-binary")
	  private UUID id;

	  @Column(name = "name", nullable = false)
	  private String name;

	  @Column(name = "address", nullable = false)
	  private String address;
	  
	  @OneToMany(fetch = FetchType.EAGER, mappedBy="doctor", cascade = CascadeType.ALL)
	  private Set<MedicationPlan> medicationPlan;
	  
	  @OneToOne(cascade = CascadeType.ALL)
	  @JoinColumn(name="account_id")
	  private Account doctorAccount;

	public Doctor(String name, String address, Set<MedicationPlan> medicationPlan, Account doctorAccount) {
		this.name = name;
		this.address = address;
		this.medicationPlan = medicationPlan;
		this.doctorAccount = doctorAccount;
	}

	public Doctor(){

	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Set<MedicationPlan> getMedicationPlan() {
		return medicationPlan;
	}

	public void setMedicationPlan(Set<MedicationPlan> medicationPlan) {
		this.medicationPlan = medicationPlan;
	}

	public Account getDoctorAccount() {
		return doctorAccount;
	}

	public void setDoctorAccount(Account doctorAccount) {
		this.doctorAccount = doctorAccount;
	}


	@Override
	public String toString() {
		return "Doctor{" +
				"id=" + id +
				", name='" + name + '\'' +
				", address='" + address + '\'' +
				", medicationPlan=" + medicationPlan +
				", doctorAccount=" + doctorAccount +
				'}';
	}
}
