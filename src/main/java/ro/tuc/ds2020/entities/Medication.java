package ro.tuc.ds2020.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity
public class Medication implements Serializable{

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Type(type = "uuid-binary")
	private UUID id;
	  
	@Column(name = "name", nullable = false)
	private String name;
	
	@Column(name = "sideEffects", nullable = false)
	private String sideEffects;
	
	@Column(name = "dosage", nullable = false)
	private String dosage;
	
	@ManyToMany(fetch = FetchType.EAGER)
	private Set<MedicationPlan> medicationPlanList;

	public Medication(String name, String sideEffects, String dosage, Set<MedicationPlan> medicationPlanList) {
		this.name = name;
		this.sideEffects = sideEffects;
		this.dosage = dosage;
		this.medicationPlanList = medicationPlanList;
	}

	public Medication(UUID id, String name, String sideEffects, String dosage) {
		this.id = id;
		this.name = name;
		this.sideEffects = sideEffects;
		this.dosage = dosage;
	}



	public Medication(){

	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSideEffects() {
		return sideEffects;
	}

	public void setSideEffects(String sideEffects) {
		this.sideEffects = sideEffects;
	}

	public String getDosage() {
		return dosage;
	}

	public void setDosage(String dosage) {
		this.dosage = dosage;
	}

	public Set<MedicationPlan> getMedicationPlanList() {
		return medicationPlanList;
	}

	public void setMedicationPlanList(Set<MedicationPlan> medicationPlanList) {
		this.medicationPlanList = medicationPlanList;
	}

	@Override
	public String toString() {
		return "Medication{" +
				"id=" + id +
				", name='" + name + '\'' +
				", sideEffects='" + sideEffects + '\'' +
				", dosage='" + dosage + '\'' +
				'}';
	}
}
