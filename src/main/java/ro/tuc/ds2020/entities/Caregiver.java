package ro.tuc.ds2020.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity
public class Caregiver implements Serializable{

	  @Id
	  @GeneratedValue(generator = "uuid2")
	  @GenericGenerator(name = "uuid2", strategy = "uuid2")
	  @Type(type = "uuid-binary")
	  private UUID id;
	  
	  @Column(name = "name", nullable = false)
	  private String name;
	  
	  @Column(name = "birthDate", nullable = false)
	  private String birthDate;
	  
	  @Column(name = "gender", nullable = false)
	  private String gender;
	  
	  @Column(name = "address", nullable = false)
	  private String address;
	  
	  @OneToMany(mappedBy = "caregiver", fetch = FetchType.EAGER)
	  private Set<Patient> patientsList;
	
	  @OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	  @JoinColumn(name="account_id")
	  private Account caregiverAccount;

	  public Caregiver(){

	  }

	public Caregiver(UUID id, String name, String birthDate, String gender, String address, Set<Patient> patientsList, Account caregiverAccount) {
		this.id = id;
		this.name = name;
		this.birthDate = birthDate;
		this.gender = gender;
		this.address = address;
		this.patientsList = patientsList;
		this.caregiverAccount = caregiverAccount;
	}

	public Caregiver(String name, String birthDate, String gender, String address, Set<Patient> patientsList, Account caregiverAccount) {
		this.name = name;
		this.birthDate = birthDate;
		this.gender = gender;
		this.address = address;
		this.patientsList = patientsList;
		this.caregiverAccount = caregiverAccount;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Set<Patient> getPatientsList() {
		return patientsList;
	}

	public void setPatientsList(Set<Patient> patientsList) {
		this.patientsList = patientsList;
	}

	public Account getCaregiverAccount() {
		return caregiverAccount;
	}

	public void setCaregiverAccount(Account caregiverAccount) {
		this.caregiverAccount = caregiverAccount;
	}


	  
}
