package ro.tuc.ds2020.entities;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity
public class Account implements Serializable{
	
	  @Id
	  @GeneratedValue(generator = "uuid2")
	  @GenericGenerator(name = "uuid2", strategy = "uuid2")
	  @Type(type = "uuid-binary")
	  private UUID id;
	  
	  @Column(name = "username", nullable = false)
	  private String username;
	  
	  @Column(name = "pwd", nullable = false)
	  private String password;
	  
	  @OneToOne(mappedBy="doctorAccount", fetch = FetchType.EAGER,cascade = {CascadeType.PERSIST,
			  CascadeType.DETACH,
			  CascadeType.REFRESH,
			  CascadeType.REMOVE})
	  private Doctor doctor;
	  
	  @OneToOne(mappedBy="patientAccount", fetch = FetchType.EAGER,cascade = {CascadeType.PERSIST,
			  CascadeType.DETACH,
			  CascadeType.REFRESH,
			  CascadeType.REMOVE})
	  private Patient patient;
	  
	  @OneToOne(mappedBy="caregiverAccount", fetch = FetchType.EAGER,cascade = {CascadeType.PERSIST,
			  CascadeType.DETACH,
			  CascadeType.REFRESH,
			  CascadeType.REMOVE})
	  private Caregiver caregiver;

	public Account(String username, String password, Doctor doctor, Patient patient, Caregiver caregiver) {
		this.username = username;
		this.password = password;
		this.doctor = doctor;
		this.patient = patient;
		this.caregiver = caregiver;
	}

	public Account(UUID id, String username, String password, Doctor doctor, Patient patient, Caregiver caregiver) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.doctor = doctor;
		this.patient = patient;
		this.caregiver = caregiver;
	}

	public Account(){

	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Caregiver getCaregiver() {
		return caregiver;
	}

	public void setCaregiver(Caregiver caregiver) {
		this.caregiver = caregiver;
	}

	@Override
	public String toString() {
		return "Account{" +
				"id=" + id +
				", username='" + username + '\'' +
				", password='" + password + '\'' +
				'}';
	}
}
