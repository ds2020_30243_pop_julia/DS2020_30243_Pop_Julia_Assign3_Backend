package ro.tuc.ds2020.servicesInterfaces;

import ro.tuc.ds2020.dtos.CaregiverDTO;

public interface ICaregiverService {
    CaregiverDTO findCaregiverByUsername(String username);
}
