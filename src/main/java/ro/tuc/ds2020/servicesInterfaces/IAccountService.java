package ro.tuc.ds2020.servicesInterfaces;

import ro.tuc.ds2020.dtos.AccountDTO;

public interface IAccountService {
    AccountDTO login(String username, String password);
}
