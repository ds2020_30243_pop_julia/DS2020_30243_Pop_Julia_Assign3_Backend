package ro.tuc.ds2020.servicesInterfaces;

import ro.tuc.ds2020.dtos.*;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface IDoctorService {

    void testAll();

    List<PatientDTO> findAllPatients();
    PatientDTO findPatientById(UUID id);
    UUID createPatient(PatientDTO patientDTO);
    UUID updatePatient(PatientDTO patientDTO);
    UUID deletePatient(UUID id);

    CaregiverDTO findCaregiverById(UUID id);
    UUID createCaregiver(CaregiverDTO caregiverDTO);
    UUID deleteCaregiver(UUID id);
    UUID updateCaregiver(CaregiverDTO caregiverDTO);
    UUID createCaregiverAccount(AccountDTO accountDTO);

    UUID createMedication(MedicationDTO medicationDTO);
    List<MedicationDTO> findAllMedications();
    MedicationDTO findMedicationById(UUID id);
    UUID updateMedication(MedicationDTO medicationDTO);
    UUID deleteMedication(UUID id);

    UUID createAccount(AccountDTO accountDTO);

    List<CaregiverDTO> findAllCaregivers();

    UUID createMedicationPlan(MedicationPlanDTO medicationPlanDTO);

    Set<PatientDTO> searchPatient(String searchString);

    Set<CaregiverDTO> searchCaregiver(String searchString);

    Set<MedicationDTO> searchMedication(String searchString);

    //List<MedicationDTO> findAllMedications();
}
