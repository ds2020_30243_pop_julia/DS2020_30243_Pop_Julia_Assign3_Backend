package ro.tuc.ds2020.servicesInterfaces;

public interface IRPCServer {

    void start() throws Exception;
}
