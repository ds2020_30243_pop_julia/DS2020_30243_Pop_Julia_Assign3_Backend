package ro.tuc.ds2020.servicesInterfaces;

import ro.tuc.ds2020.dtos.PatientDTO;

public interface IPatientService {
    PatientDTO findPatientByUsername(String username);
}
