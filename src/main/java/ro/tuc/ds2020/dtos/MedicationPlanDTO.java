package ro.tuc.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.util.List;

public class MedicationPlanDTO {

    @NotNull
    private String intakeIntervals;
    @NotNull
    private String treatmentPeriod;
    @NotNull
    private String doctorName;
    @NotNull
    private String patientName;
    @NotNull
    private List<String> medicationNames;

    public MedicationPlanDTO(@NotNull String intakeIntervals, @NotNull String treatmentPeriod, @NotNull String doctorName, @NotNull String patientName, @NotNull List<String> medicationNames) {
        this.intakeIntervals = intakeIntervals;
        this.treatmentPeriod = treatmentPeriod;
        this.doctorName = doctorName;
        this.patientName = patientName;
        this.medicationNames = medicationNames;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public String getTreatmentPeriod() {
        return treatmentPeriod;
    }

    public void setTreatmentPeriod(String treatmentPeriod) {
        this.treatmentPeriod = treatmentPeriod;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public List<String> getMedicationNames() {
        return medicationNames;
    }

    public void setMedicationNames(List<String> medicationName) {
        this.medicationNames = medicationName;
    }

    @Override
    public String toString() {
        return "MedicationPlanDTO{" +
                "intakeIntervals='" + intakeIntervals + '\'' +
                ", treatmentPeriod='" + treatmentPeriod + '\'' +
                ", doctorName='" + doctorName + '\'' +
                ", patientName='" + patientName + '\'' +
                ", medicationNames=" + medicationNames +
                '}';
    }
}
