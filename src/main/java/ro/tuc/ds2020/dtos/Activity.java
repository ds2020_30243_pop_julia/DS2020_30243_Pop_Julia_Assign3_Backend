package ro.tuc.ds2020.dtos;

import java.util.UUID;

public class Activity {

    UUID patient_id;
    String activity;
    String start;
    String end;

    public Activity() {

    }

    public Activity(UUID patient_id, String activity, String start, String end) {
        super();
        this.patient_id = patient_id;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }
    public UUID getPatient_id() {
        return patient_id;
    }
    public void setPatient_id(UUID patient_id) {
        this.patient_id = patient_id;
    }
    public String getActivity() {
        return activity;
    }
    public void setActivity(String activity) {
        this.activity = activity;
    }
    public String getStart() {
        return start;
    }
    public void setStart(String start) {
        this.start = start;
    }
    public String getEnd() {
        return end;
    }
    public void setEnd(String end) {
        this.end = end;
    }
    @Override
    public String toString() {
        return "Activity [patient_id=" + patient_id + ", activity=" + activity + ", start=" + start + ", end=" + end
                + "]";
    }


}
