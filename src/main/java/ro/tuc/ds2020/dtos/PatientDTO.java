package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class PatientDTO extends RepresentationModel<PatientDTO> {
    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private String birthDate;
    @NotNull
    private String gender;
    @NotNull
    private String address;
    private String medicalRecord;
    private Set<MedicationPlanDTO> medicationPlansDTO;
    private UUID accountId;
    private String caregiverName;

    public PatientDTO(){

    }

    public PatientDTO(UUID id, String name, String birthDate, String gender, String address, String medicalRecord, Set<MedicationPlanDTO> medicationPlansDTO, UUID accountId, String caregiverName) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.medicationPlansDTO = medicationPlansDTO;
        this.accountId = accountId;
        this.caregiverName = caregiverName;
    }

    public PatientDTO(String name, String birthDate, String gender, String address, String medicalRecord, Set<MedicationPlanDTO> medicationPlansDTO, UUID accountId, String caregiverName) {
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.medicationPlansDTO = medicationPlansDTO;
        this.accountId = accountId;
        this.caregiverName = caregiverName;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public Set<MedicationPlanDTO> getMedicationPlansDTO() {
        return medicationPlansDTO;
    }

    public void setMedicationPlansDTO(Set<MedicationPlanDTO> medicationPlansDTO) {
        this.medicationPlansDTO = medicationPlansDTO;
    }

    public UUID getAccountId() {
        return accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public String getCaregiverName() {
        return caregiverName;
    }

    public void setCaregiverName(String caregiverName) {
        this.caregiverName = caregiverName;
    }

    @Override
    public String toString() {
        return "PatientDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", gender='" + gender + '\'' +
                ", address='" + address + '\'' +
                ", medicalRecord='" + medicalRecord + '\'' +
                ", medicationPlansDTO=" + medicationPlansDTO +
                ", accountId=" + accountId +
                ", caregiverName='" + caregiverName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PatientDTO that = (PatientDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, name);
    }
}
