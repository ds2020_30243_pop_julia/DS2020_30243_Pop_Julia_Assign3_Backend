package ro.tuc.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class AccountDTO {

    private UUID id;
    @NotNull
    private String username;
    @NotNull
    private String password;
    private UUID ownerId;
    private String type;

    public AccountDTO(UUID id, @NotNull String username, @NotNull String password, UUID ownerId, String type) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.ownerId = ownerId;
        this.type = type;
    }

    public AccountDTO(@NotNull String username, @NotNull String password, UUID ownerId, String type) {
        this.username = username;
        this.password = password;
        this.ownerId = ownerId;
        this.type = type;
    }

    public AccountDTO()
    {

    }
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UUID getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(UUID ownerId) {
        this.ownerId = ownerId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "AccountDTO{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", ownerId=" + ownerId +
                ", type='" + type + '\'' +
                '}';
    }
}
