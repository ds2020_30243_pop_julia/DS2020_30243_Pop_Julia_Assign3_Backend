package ro.tuc.ds2020.dtos.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.AccountRepository;
import ro.tuc.ds2020.repositories.CaregiverRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
public class CaregiverBuilder {
    @Autowired
    PatientRepository patientRepository;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    PatientBuilder patientBuilder;

    public CaregiverDTO toCaregiverDTO(Caregiver caregiver) {
        Account account = null;
        Set<String> patientDTOSet = new HashSet<String>();
        UUID id = null;
        if(caregiver.getCaregiverAccount() != null){
            id = caregiver.getCaregiverAccount().getId();
        }

        for(Patient p : caregiver.getPatientsList()){
            System.out.println(p);
            patientDTOSet.add(p.getName());
        }
        return new CaregiverDTO(caregiver.getId(),
                caregiver.getName(),
                caregiver.getBirthDate(),
                caregiver.getGender(),
                caregiver.getAddress(),
                patientDTOSet,
                id);
    }

    public Caregiver toEntity(CaregiverDTO caregiverDTO){
        Account account = null;
        if(caregiverDTO.getAccountId() != null){
            Optional<Account> optionalAccount = accountRepository.findById(caregiverDTO.getAccountId());
            account = optionalAccount.get();
        }

        Set<Patient> patients = new HashSet<Patient>();

        if(caregiverDTO.getPatients() != null){
            for(String p: caregiverDTO.getPatients()){
                patients.add(patientRepository.findByName(p));
            }
        }

        return new Caregiver(caregiverDTO.getId(),
                caregiverDTO.getName(),
                caregiverDTO.getBirthDate(),
                caregiverDTO.getGender(),
                caregiverDTO.getAddress(),
                patients,
                account);
    }

}
