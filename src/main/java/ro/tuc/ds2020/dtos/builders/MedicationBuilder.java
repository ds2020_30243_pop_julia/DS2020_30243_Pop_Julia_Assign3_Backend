package ro.tuc.ds2020.dtos.builders;

import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.entities.Medication;

@Service
public class MedicationBuilder {

    public MedicationDTO toMedicationDTO(Medication medication){
        return new MedicationDTO(medication.getId(),
                medication.getName(),
                medication.getSideEffects(),
                medication.getDosage()
        );
    }

    public Medication toEntity(MedicationDTO medicationDTO){
        return new Medication(medicationDTO.getId(),
                medicationDTO.getName(),
                medicationDTO.getSideEffects(),
                medicationDTO.getDosage()
        );

    }
}
