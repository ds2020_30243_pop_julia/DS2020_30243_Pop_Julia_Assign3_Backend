package ro.tuc.ds2020.dtos.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.AccountDTO;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.CaregiverRepository;
import ro.tuc.ds2020.repositories.DoctorRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.Optional;
import java.util.UUID;

@Service
public class AccountBuilder {
    @Autowired
    DoctorRepository doctorRepository;
    @Autowired
    CaregiverRepository caregiverRepository;
    @Autowired
    PatientRepository patientRepository;

    public AccountDTO toAccountDTO(Account account) {
        UUID ownerId = null;
        String type = "";
        if(account.getDoctor() != null){
            ownerId = account.getDoctor().getId();
            type = "doctor";
        } else if(account.getCaregiver() != null){
            ownerId = account.getCaregiver().getId();
            type = "caregiver";
        } else if(account.getPatient() != null){
            ownerId = account.getPatient().getId();
            type = "patient";
        }
        return new AccountDTO(account.getId(),
                account.getUsername(),
                account.getPassword(),
                ownerId,
                type);
    }

    public Account toEntity(AccountDTO accountDTO){
        Account account = new Account(accountDTO.getId(),
                accountDTO.getUsername(),
                accountDTO.getPassword(),
                null,
                null,
                null);

        switch(accountDTO.getType()) {

            case "doctor" :
                Optional<Doctor> optionalDoctor = doctorRepository.findById(accountDTO.getOwnerId());
                if(optionalDoctor.isPresent()){
                    account.setDoctor(optionalDoctor.get());
                }
                break;
            case "patient":
                Optional<Patient> optionalPatient = patientRepository.findById(accountDTO.getOwnerId());
                if(optionalPatient.isPresent()){
                    account.setPatient(optionalPatient.get());
                }
                break;
            case "caregiver":
                Optional<Caregiver> optionalCaregiver = caregiverRepository.findById(accountDTO.getOwnerId());
                if(optionalCaregiver.isPresent()){
                    account.setCaregiver((optionalCaregiver.get()));
                }
				break;
			default: break;


        }
        return account;

    }
}
