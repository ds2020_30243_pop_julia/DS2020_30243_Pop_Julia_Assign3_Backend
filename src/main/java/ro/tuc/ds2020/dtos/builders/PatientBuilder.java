package ro.tuc.ds2020.dtos.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.Account;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.AccountRepository;
import ro.tuc.ds2020.repositories.CaregiverRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
public class PatientBuilder {


    @Autowired
    CaregiverRepository caregiverRepository;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    MedicationPlanBuilder medicationPlanBuilder;

    /*
    public PatientBuilder(CaregiverRepository caregiverRepository,AccountRepository accountRepository ) {
        this.caregiverRepository = caregiverRepository;
        this.accountRepository = accountRepository;
    }*/

    public PatientDTO toPatientDTO(Patient patient) {
        //System.out.println("Convertiesc");
       Set<MedicationPlanDTO> medicationPlanDTOSet = new HashSet<MedicationPlanDTO>();
        UUID id = null;
        String caregiverName = null;
       //medicationPlanBuilder = new MedicationPlanBuilder();
        for(MedicationPlan mp : patient.getPatientMedicationPlans()){
            medicationPlanDTOSet.add(medicationPlanBuilder.toMedicationPlanDTO(mp));
        }
        if(patient.getPatientAccount() != null){
            id = patient.getPatientAccount().getId();
        }
        if(patient.getCaregiver() != null){
            caregiverName = patient.getCaregiver().getName();
        }
       return new PatientDTO(patient.getId(),
                patient.getName(),
                patient.getBirthDate(),
                patient.getGender(),
                patient.getAddress(),
                patient.getMedicalRecord(),
                medicationPlanDTOSet,
                id,
                caregiverName);
    }

    public Patient toEntity(PatientDTO patientDTO){
        Optional<Account> optionalAccount = null;
        Set<MedicationPlan> medicationPlans = new HashSet<MedicationPlan>();
        Optional<Caregiver> optionalCaregiver = null;

        Account account = null;
        Caregiver caregiver = null;

        if(patientDTO.getCaregiverName() != null){
            optionalCaregiver = caregiverRepository.findByName(patientDTO.getCaregiverName());
            if(optionalCaregiver.isPresent())
            caregiver = optionalCaregiver.get();

        }
        if(patientDTO.getAccountId() != null) {
            optionalAccount = accountRepository.findById(patientDTO.getAccountId());
            if(optionalAccount.isPresent())
            account = optionalAccount.get();
        }

        //medicationPlanBuilder = new MedicationPlanBuilder();
        if(patientDTO.getMedicationPlansDTO() != null) {
            for (MedicationPlanDTO mp : patientDTO.getMedicationPlansDTO()) {
                medicationPlans.add(medicationPlanBuilder.toEntity(mp));
            }
        }
        return new Patient(patientDTO.getId(),
                patientDTO.getName(),
                patientDTO.getBirthDate(),
                patientDTO.getGender(),
                patientDTO.getAddress(),
                patientDTO.getMedicalRecord(),
                medicationPlans,
                account,
                caregiver
        );

    }



}
