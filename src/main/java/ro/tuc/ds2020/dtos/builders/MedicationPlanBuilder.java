package ro.tuc.ds2020.dtos.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.entities.*;
import ro.tuc.ds2020.repositories.AccountRepository;
import ro.tuc.ds2020.repositories.DoctorRepository;
import ro.tuc.ds2020.repositories.MedicationRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.*;

@Service
public class MedicationPlanBuilder {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    DoctorRepository doctorRepository;

    @Autowired
    MedicationRepository medicationRepository;

    @Autowired
    PatientRepository patientRepository;

    public MedicationPlanBuilder(){

    }

    public MedicationPlanDTO toMedicationPlanDTO(MedicationPlan medicationPlan){
        List<String> medicationsList = new ArrayList<String>();
        for(Medication m : medicationPlan.getMedicationList()){
            medicationsList.add(m.getName());
        }
        return new MedicationPlanDTO( medicationPlan.getIntakeIntervals(),
                medicationPlan.getTreatmentPeriod(),
                medicationPlan.getDoctor().getName(),
                medicationPlan.getPatient().getName(),
                medicationsList
        );
    }

    public MedicationPlan toEntity(MedicationPlanDTO medicationPlanDTO){
        System.out.println(doctorRepository.findAll());
        System.out.println(patientRepository.findAll());
        Doctor doctor = null;
        if(!medicationPlanDTO.getDoctorName().isEmpty())
            doctor = doctorRepository.findByName(medicationPlanDTO.getDoctorName());
        Account doctorAccount = accountRepository.findByUsername(medicationPlanDTO.getDoctorName());
        if(doctorAccount != null)
            doctor = doctorAccount.getDoctor();
        Patient patient = patientRepository.findByName(medicationPlanDTO.getPatientName());
        Set<Medication> medicationSet = new HashSet<Medication>();
        for(String m : medicationPlanDTO.getMedicationNames()){
            medicationSet.add(medicationRepository.findByName(m));
        }

        return new MedicationPlan(
                medicationPlanDTO.getIntakeIntervals(),
                medicationPlanDTO.getTreatmentPeriod(),
                doctor,
                patient,
                medicationSet);
    }
}
